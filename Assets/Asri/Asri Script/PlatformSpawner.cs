﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour {

	public GameObject[] obstacles;
	public GameObject[] spawnPosition;
    public GameObject spawner;

	public float spawnRate = 0.75f;
	public float nextSpawn = 0.0f;


	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

		if (Time.time >= nextSpawn)
		{
			nextSpawn = Time.time + spawnRate;

			int selection = Random.Range(0, obstacles.Length);

			Vector3 pos = new Vector3(transform.position.x, Random.Range(-1, 2), 0);
			Instantiate(obstacles[selection], pos, Quaternion.identity);

		}
	}
}
