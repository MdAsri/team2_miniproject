﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public AudioClip audioClip;
    public AudioSource musicBackground;

    private void Start()
    {
        musicBackground.clip = audioClip;
        musicBackground.Play();
    }

    public void PlayGame()
	{
		SceneManager.LoadScene("MainScene");
	}
		
	public void QuitGame()
	{
		Application.Quit ();
		print("Quit Program!");
	}
}
