﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour {

	private float width = 0f;

	void Start()
	{
		width = GameObject.Find("BG").GetComponent<BoxCollider>().size.x;
	}

	void OnTriggerEnter(Collider target)
	{
		if(target.tag == "BG" || target.tag == "Background" )
		{
			Vector3 temp = target.transform.position;
			temp.x += width * 3.71f;
			target.transform.position = temp;
		}
	}
}
