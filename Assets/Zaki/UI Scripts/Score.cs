﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class Score : MonoBehaviour {

	private float score = 0.0f;
	public Text scoreText;
    public DeathMenu deathMenu;

	private bool isDead = false;
	
	// Update is called once per frame
	void Update () {
		if (isDead)
			return;

		score += Time.deltaTime;
		scoreText.text = ((int)score).ToString();

		

	}

	public void OnDeath()
	{
		print("OnDeath is working");
		isDead = true;
        deathMenu.ToggleEndMenu (score);
	}
}

