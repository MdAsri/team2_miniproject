﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;


public class DeathMenu : MonoBehaviour {
    
    public Text ScoreText;

	// Use this for initialization
	void Start () {
        gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ToggleEndMenu(float score)
    {
        gameObject.SetActive(true);
        ScoreText.text = ((int)score).ToString();
    }

    public void Restart()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void ToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
