﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScoreDemo : MonoBehaviour {

	private bool isDead = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (gameObject == null)
		{
			print("Player is dead");
			Death();
		}
			

		if (isDead)
			return;
	}


	public void Death()
	{
		isDead = true;
		GetComponent<Score>().OnDeath();
	}
}
