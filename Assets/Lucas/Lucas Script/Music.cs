﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public AudioClip audioClip;
    public AudioSource musicBackground;

    private void Start()
    {
        musicBackground.clip = audioClip;
        musicBackground.Play();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
