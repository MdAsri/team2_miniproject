using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
	private Rigidbody2D rb;
	private Animator anim;
	private bool facingRight = true;
	private Vector3 localScale;

	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		localScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetButtonDown ("Jump") && rb.velocity.y == 0)
		{
			rb.AddForce (Vector2.up * 600f);
		}

		if (rb.velocity.y == 0)
		{
			anim.SetBool ("isJumping", false);
			anim.SetBool ("isFalling", false);
		}

		if (rb.velocity.y > 0)
		{
			anim.SetBool ("isJumping", true);
		}

		if (rb.velocity.y < 0)
		{
			anim.SetBool ("isJumping", false);
			anim.SetBool ("isFalling", true);
		}
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Death")
        {
            Destroy(gameObject);
            PlayerScoreDemo psd = gameObject.GetComponent<PlayerScoreDemo>();
            psd.Death();
        }
    }
}
